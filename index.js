/* NodeJS Custom modules */
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

/* Custom application libraries */
const routes = require('./libs/routes');

const app = express();
const port = process.env.port || 3000;

app.set('view engine','ejs');
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(routes);

app.listen(port,() => {
    console.log('Server running at port: ',port);
});