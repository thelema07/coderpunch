
const express = require('express');

const router = express.Router();

const index = router.route('/');

index.get((req,res) => {
    res.render('pages/index');
});

const about = router.route('/about');

about.get((req,res) => {
    res.render('pages/about');
});

module.exports = router;